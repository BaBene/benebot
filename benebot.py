import socket
import re
from datetime import datetime

class Bot():

    def __init__(self, username, password, channels):
        self.username = username
        self.password = password
        self.channels = {channel: {"limit": limit, "timestamps": []} for channel, limit in channels}
        self.commands = []
        self.socket = None

    def connect(self):
        if self.socket is None:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.connect(("irc.chat.twitch.tv", 6667))

            self.sendPassword()
            self.sendUsername()

            print("Connected")

            for channel in self.channels:
                self.joinChannel(channel)



    def sendUsername(self):
        if self.socket is not None:
            self.socket.sendall("NICK {}\r\n".format(self.username))

    def sendPassword(self):
        if self.socket is not None:
            self.socket.sendall("PASS {}\r\n".format(self.password))

    def joinChannel(self, channel):
        if self.socket is not None:
            self.socket.sendall("JOIN {}\r\n".format(channel))
            print("Joined channel {}".format(channel))

    def sendPong(self, server):
        if self.socket is not None:
            self.socket.sendall("PONG {}\r\n".format(server))
            print("Received PING. PONG!")

    def onMessage(self, nickname, channel, message):

        for command in self.commands:
            current_time = datetime.now()


            if command["nicknames"] is not None and nickname not in command["nicknames"]:
                continue
            if command["channels"] is not None and channel not in command["channels"]:
                continue
            if command["cooldown"] is not None and channel in command["last_called"] and (current_time - command["last_called"][channel]).total_seconds() < command["cooldown"]:
                continue

            if command["starts_with"]:
                regex = "^(?P<trigger>{})(?P<args>.*)".format(command["trigger"])
            else:
                regex = ".*(?P<trigger>{})(?P<args>.*)".format(command["trigger"])

            match = re.match(regex, message, flags=0 if command["case_sensitive"] else re.IGNORECASE)

            if match is not None:
                command["function"](self, nickname, channel, match.group("args").strip().split(" "), message)
                command["last_called"][channel] = current_time
                print("Ran command {}, triggered by {} in channel {}".format(command["trigger"], nickname, channel))
            

    def sendMessage(self, channel, message):
        if self.socket is not None:
            if channel not in self.channels:
                print("Channel not joined")
            else:
                current_time = datetime.now()

                self.channels[channel]["timestamps"][:] = [x for x in self.channels[channel]["timestamps"] if (current_time - x).total_seconds() <= 30]

                if len(self.channels[channel]["timestamps"]) < self.channels[channel]["limit"]:
                    self.socket.sendall("PRIVMSG {} :{}\r\n".format(channel, message))
                    self.channels[channel]["timestamps"].append(current_time)

    def addCommand(self, function, trigger, nicknames=None, channels=None, case_sensitive=False, starts_with=True, cooldown=None):
        self.commands.append({"function": function, "trigger": trigger, "nicknames": nicknames, "channels": channels, "case_sensitive": case_sensitive, "starts_with": starts_with, "cooldown": cooldown, "last_called": {}})
        

    def run(self):
        while self.socket is not None:
            data = self.socket.recv(4096)

            string = data.strip()


            if string.startswith("PING"):
                server = re.match("PING (?P<server>.*)", string).group("server")
                self.sendPong(server)
            else:
                match = re.match(":(?P<nickname>[a-zA-Z0-9_]+)!(?P=nickname)@(?P=nickname).tmi.twitch.tv (?P<action>[A-Z]+?) (?P<content>.*)", string)

                if match is not None:
                    if match.group("action") == "PRIVMSG":
                        privmsg = re.match("(?P<channel>#[a-zA-Z0-9_]+) :(?P<message>.+)", match.group("content"))

                        self.onMessage(match.group("nickname"), privmsg.group("channel"), privmsg.group("message"))


            

    def __del__(self):
        if self.socket is not None:
            print("Closing connection")
            self.socket.close()


############## USER COMMANDS ###################
# The signature of the functions must always be:
#   bot: A reference to the bot, basically used for writing to a channel (e.g. bot.sendMessage(channel, message)
#   nickname: The nickname of the user that triggered the command
#   channel: The channel in which the user triggered the command
#   args: An array of all the words (seperated by a whitespace) that followed the trigger command
#   message: Contains the original message
def exampleCommand1(bot, nickname, channel, args, message):
    bot.sendMessage(channel, "BeneSim is awesome!")

def exampleCommand2(bot, nickname, channel, args, message):
    bot.sendMessage(channel, "Save the Children is even more awesome!")
    

if __name__ == "__main__":
    # Initialize the bot, requires a valid username and password
    # Channels must be an iterable (e.g. list, tuple ...) containing another iterable with two entries.
    # The first entry is supposed to be the channelname and the second the message limiter.
    # The message limiter dictates how many messages can be send within 30s.
    bot = Bot(username="botusername", password="oauth:botpassword", channels=(("#beneflight", 20),("#benesim", 20)))

    # Add the commands to the bot

    # Example of a command that triggers in every channel for every nickname and doesn't need to be the start of a sentence but has a cooldown of 10s
    bot.addCommand(exampleCommand1, "benesim", starts_with=False, cooldown=10)
    # Example of a command that only triggers for the nickname "benesim" in the channels "#benesim" and "#beneflight" and
    # it has to be the start of a sentence without a cooldown
    bot.addCommand(exampleCommand2, "children", nicknames=("benesim",), channels=("#foo", "#bar"), case_sensitive=False, starts_with=True)

    bot.connect()
    bot.run()
